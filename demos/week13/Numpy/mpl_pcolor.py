import numpy as np
import matplotlib.pylab as plt
import np_poisson as fish

# get solution
N = 200
Niter = 10000
u = fish.calc(N, Niter, func=fish.numpy_update)

# pseudo-color plot
x = np.linspace(0., 1., N)
plt.pcolormesh(x, x, u)

# plot cosmetics
plt.xlabel(r"$x$")
plt.ylabel(r"$y$")
plt.title("Solution")
plt.axis('scaled')

# save the figure
plt.savefig("solution.png")

# show
plt.show()

