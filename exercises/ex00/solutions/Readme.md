# Exercise 0

Compile using the compiler directly
```
c++ -Wall -Wextra -Wpedantic gcd.cpp        -o gcd
c++ -Wall -Wextra -Wpedantic fibonacci.cpp  -o fibonacci
c++ -Wall -Wextra -Wpedantic merge_sort.cpp -o merge_sort
```

Compile using Make (discussed in week 02 of the lecture)
```
make
```

Compile using CMake (discussed in week 03 of the lecture)

```
mkdir -p build
cd build
cmake ..
cmake --build .
```
