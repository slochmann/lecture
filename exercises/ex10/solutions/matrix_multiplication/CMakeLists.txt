# Programming Techniques for Scientific Simulations I
# HS 2021
# Exercise 10

cmake_minimum_required (VERSION 3.15)
project (ex10-matrix-multiplication)

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)
set (CMAKE_CXX_EXTENSIONS FALSE)

add_compile_options (-Wall -Wextra -Wpedantic -march=native)

add_executable (main main.cpp mm0.cpp mm1.cpp mm2.cpp mm3.cpp mm_blas.cpp mm_eigen.cpp)

find_package (BLAS REQUIRED)
target_link_libraries (main ${BLAS_LINKER_FLAGS} ${BLAS_LIBRARIES})

find_package (Eigen3 REQUIRED NO_MODULE)
target_link_libraries (main Eigen3::Eigen)
