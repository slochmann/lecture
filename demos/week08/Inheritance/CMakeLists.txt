cmake_minimum_required(VERSION 3.1)

project(PT1_week07_Inheritance)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

# setting warning compiler flags
add_compile_options(-Wall -Wextra -Wpedantic)

add_executable(inheritance01 inheritance01.cpp)
add_executable(inheritance02 inheritance02.cpp)
add_executable(inheritance03 inheritance03.cpp)
add_executable(inheritance04 inheritance04.cpp)
add_executable(inheritance05 inheritance05.cpp)
add_executable(inheritance06 inheritance06.cpp)
add_executable(virtual_dtor virtual_dtor.cpp)
