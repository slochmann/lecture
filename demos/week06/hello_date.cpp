#include <iostream>
#include <ctime>

int main() {
  std::cout << "Hello ETH students!\n";
  std::time_t current_time = std::time(0); // get current time
  std::cout << "It is now: " << std::ctime( &current_time )
            << '\n'; // print time
}
