import math

# open file and write some stuff
f = open("dummy.file",'w')
f.write("What I loose in sense I gain in poetry!\n")
f.write("e  = {}\n".format(math.pi))
f.write("pi = {}\n".format(math.pi))
f.close()

# open file, read, check if it s closed and print it!
with open("dummy.file", 'r') as f:
    lines = f.readlines()

print("Is the file closed?", f.closed)

for line in lines:
    print(line.strip()) # strip gets rid of \n's!
