#include <iostream>
#include <stdexcept>

namespace Stack {

  class stack {
    public:
      stack(int l) : s(new double[l]), p(s), n(l) {} // ctor
      ~stack() { delete[] s; }                       // dtor

      void push(double v) {
        if ( p == s + n - 1 ) {
          throw std::runtime_error("stack overflow");
        }
        *p++ = v;
      }

      double pop() {
        if ( p == s ) {
          throw std::runtime_error("stack underflow");
        }
        return *--p;
      }
    private:
      double* s; // pointer to allocated stack memory
      double* p; // stack pointer
      int n;     // stack size
  };

}

int main() {
  try {
    Stack::stack s(100);
    s.push(10.);
    std::cout << s.pop() << '\n';
    std::cout << s.pop() << '\n'; // throws error
  }
  catch (std::exception& e) {
    std::cerr << "Exception occurred: " << e.what() << '\n';
    return 1; // return error, i.e. non-zero value!
  }

  return 0; // return 0, i.e. 0 for zero problems!
}
