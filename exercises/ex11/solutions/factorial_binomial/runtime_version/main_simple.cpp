/*
 * Programming Techniques for Scientific Simulations I
 * HS 2018
 * Week 11
 */

#include <iostream>
#include <iomanip>

size_t factorial(const size_t & value)
{
    if(value == 0) {
        return 1;
    }
    return value * factorial(value - 1);
}

size_t binomial(const size_t & N, const size_t & k)
{
    return factorial(N) / (factorial(N-k) * factorial(k));
}


int main()
{
    std::cout << "Factorial:" << std::endl;
    for(size_t N = 0; N < 10; ++N) {
        std::cout << "    " << N << "! = " << factorial(N) << std::endl;
    }

    std::cout << "Binomial:" << std::endl;
    for(size_t N = 1; N < 10; ++N) {
        std::cout << std::setw(5) << "N=" << N ;
        for(size_t k = 0; k <= N; ++k) {
            std::cout << "    " << std::setw(5) << binomial(N, k);
        }
        std::cout << std::endl;
    }

    std::cout << "Limits:" << std::endl;
    std::cout << "    20! = " << factorial(20) << std::endl;
    std::cout << "    21! = " << factorial(21) << std::endl;
    std::cout << "    70! = " << factorial(70) << std::endl;

    std::cout << "Limits:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << binomial(20,1) << std::endl;
    std::cout << "    N=21, k=1 ==> " << binomial(21,1) << " <- wrong " << std::endl;
    // will trigger floating point exception
    // std::cout << "    N=70, k=1 ==> " << binomial(70,1) << std::endl;

    return 0;
}
